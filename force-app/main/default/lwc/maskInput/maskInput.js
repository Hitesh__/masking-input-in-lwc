import {
    LightningElement,
    track,
    api
} from 'lwc';

export default class MaskInput extends LightningElement {

    @track creditCardNumber;
    
    showData(event) {
        if (event.target.name === 'cardNumber') {
            event.target.value = this.creditCardNumber;
        }
    }

    handleCardNumber(event) {

        let _value = event.target.value;
        this.creditCardNumber = event.target.value;
        
        if (_value) {
            _value = _value.replace(/-/g, "");
            if (_value.length > 12) {

                let val1 = _value.substring(12);
                this.template.querySelector(`[data-field=${event.target.dataset.field}]`).value = '****-****-****-' + val1;

            } else if (_value.length > 8) {

                let val1 = _value.substring(8);
                this.template.querySelector(`[data-field=${event.target.dataset.field}]`).value = '****-****-' + this.formMasks(val1);

            } else if (_value.length > 4) {

                let val1 = _value.substring(4);
                this.template.querySelector(`[data-field=${event.target.dataset.field}]`).value = '****-' + this.formMasks(val1);

            } else {
                this.template.querySelector(`[data-field=${event.target.dataset.field}]`).value = this.formMasks(_value);
            }

        } else {
            this.template.querySelector(`[data-field=${event.target.dataset.field}]`).value = null;
        }
    }

    formMasks(number) {
        let str = '';
        for (let index = 0; index < number.length; index++) {
            str += '*';
        }
        return str;
    }
}